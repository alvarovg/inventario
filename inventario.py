inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # inventario[codigo] = [nombre, precio, cantidad]
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    for item in inventario:
        print(f"{item}: {inventario[item]['nombre']}, precio: {inventario[item]['precio']}, cantidad: {inventario[item]['cantidad']}")

#insertar("Zapas", "nike", 110, 1)

def consultar(codigo: str):
    print(f"{codigo}: {inventario[codigo]['nombre']}, precio: {inventario[codigo]['precio']}, cantidad: {inventario[codigo]['cantidad']}")

def agotados():
    for item in inventario:
        if inventario[item]["cantidad"] == 0:
            print(f"El articulo {item} se ha agotado")

def pide_articulo() -> (str, str, float, int):
    codigo = input("Código de artículo: ")
    nombre = input("Nombre: ")
    precio = float(input("Precio: "))
    cantidad = int(input("Cantidad: "))
    return codigo, nombre, precio, cantidad


def menu() -> int:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
        elif opcion == "2":
            listar()
        elif opcion == "3":
            codigo = str(input("Código del artículo a consultar: "))
            consultar(codigo)
        elif opcion == "4":
            agotados()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()